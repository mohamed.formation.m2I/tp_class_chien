export default class Dog {
    static count = 0

    constructor(name, breed, age) {
        this.name = name
        this.breed = breed
        this.age = age
        this.id = ++Dog.count
    }
}