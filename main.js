import Dog from "./Dog.js";


let myDogs = [
    new Dog("Bernie", "German Shepard", 8),
    new Dog("Rex", "Doberman", 4)
]

const select = document.getElementById('dog-select');
const output = document.getElementById('selectOutput');
const btn = document.getElementById('btn-submit')

const refreshDogSelect = () => {
    select.innerHTML = `<option value="0">Sélectionnez un chien</option>`;
    myDogs.forEach(chien => {
        console.log(chien);
        select.innerHTML += `<option value="${chien.id}">${chien.name}</option>`;
    });
}



btn.addEventListener("click", () => {
    let dogName = document.getElementById("dog-name").value;
    let dogBreed = document.getElementById('dog-breed').value;
    let dogAge = document.getElementById('dog-age').value;

    let newDog = new Dog(
        dogName,
        dogBreed,
        dogAge
    );

    myDogs.push(newDog)
    refreshDogSelect()
})


select.addEventListener("change", () => {
    let dogID = select.value;
    console.log(dogID);
    output.textContent = `Vous avez sélectionné le chien avec l'ID : ${dogID}`;
})


refreshDogSelect()